const basePath = `./src`
const path = require(`path`)
const paths = {
  entry: `./src/index.js`,
  output: {
    path: path.resolve(__dirname, `./dist`),
    filename: `app.bundle.js`,
  },
}

const resolve = {
  resolve: {
    extensions: [`.jsx`, `.js`, `.json`],
    alias: {},
  },
}

const rules = [
  {
    test: /\.(js)x?$/,
    exclude: /node_modules/,
    use: {
      loader: `babel-loader`,
      options: {
        babelrc: true,
      },
    },
  },
]
const devServer = {
  contentBase: "./src",
  proxy: {
    "/api": {
      target: "https://www.metaweather.com/api",
      secure: false,
      changeOrigin: true,
    },
  },
  historyApiFallback: {
    index: "index.html",
  },
}

module.exports = {
  ...paths,
  ...resolve,
  module: { rules },
  devServer,
}
