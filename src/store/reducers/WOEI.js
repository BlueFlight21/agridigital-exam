import {
  UPDATE_WHERE_ON_EARTH,
  UPDATE_WHERE_ON_EARTH_VIA_ID,
} from "../types/WOEI"
const initialState = {}
export function WOEI(state = {}, { type, whereOnEarth, id }) {
  switch (type) {
    case UPDATE_WHERE_ON_EARTH:
      return {
        ...state,
        ...whereOnEarth,
      }
    case UPDATE_WHERE_ON_EARTH_VIA_ID:
      return { ...state, [id]: { ...whereOnEarth, loaded: true } }
    default: {
      return state
    }
  }
}
