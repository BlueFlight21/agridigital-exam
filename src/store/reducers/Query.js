import { UPDATE_QUERY } from "../types/Query"
const initialState = {}
export function Query(state = {}, { type, newQuery }) {
  switch (type) {
    case UPDATE_QUERY:
      return {
        ...state,
        ...newQuery,
      }
    default: {
      return state
    }
  }
}
