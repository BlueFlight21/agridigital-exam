import { combineReducers } from "redux"
import { Query } from "./Query"
import { WOEI } from "./WOEI"

const Reducers = combineReducers({ Query, WOEI })

export default Reducers
