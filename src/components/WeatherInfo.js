import React from "react"
import styled from "styled-components"
import { getDay } from "date-fns"

const days = [
  "Sunday",
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday",
]

const StyledWeatherInfo = styled.li`
  display: flex;
  flex-direction: column;
  border: thin solid #bfbfbf;
  padding: 1em;
  h4 {
    margin: 0 0 1em;
  }
  .title {
    color: #757575;
  }
  .image-container {
    text-align: center;
    margin: 1.5em;
    > img {
      max-width: 10em;
    }
  }
`

export default function WeatherInfo(props) {
  const {
    applicable_date,
    humidity,
    wind_direction,
    wind_direction_compass,
    max_temp,
    min_temp,
    weather_state_name,
    weather_state_abbr,
  } = props
  return (
    <StyledWeatherInfo>
      <h4>{days[getDay(applicable_date)]}</h4>
      <div className="image-container">
        <img
          src={`https://www.metaweather.com/static/img/weather/${weather_state_abbr}.svg`}
          alt={weather_state_name}
        />
      </div>
      <div>
        <span className="title">Humidity: </span> {humidity}%
      </div>
      <div>
        <span className="title">Wind:</span>
        {wind_direction.toFixed(2)}° {wind_direction_compass}
      </div>
      <div>
        <span className="title">Minimum Temp: </span> {min_temp.toFixed(2)}° C
      </div>
      <div>
        <span className="title">Maximum Temp:</span> {max_temp.toFixed(2)}° C
      </div>
      <div>
        <span className="title">Skies:</span> {weather_state_name}
      </div>
    </StyledWeatherInfo>
  )
}
