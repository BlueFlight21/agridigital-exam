import React, { createRef } from "react"
import styled from "styled-components"
import { fetchList } from "./Fetch"
import { connect } from "react-redux"
import { UPDATE_QUERY } from "../store/types/Query"
import { UPDATE_WHERE_ON_EARTH } from "../store/types/WOEI"
const StyledSearch = styled.div`
  display: flex;
  flex-flow: column;
  > input {
    flex: 0.3 0 auto;
    width: 100%;
    min-height: 2em;
    @media (min-width: 1025px) {
      width: 30%;
    }
  }
  > div {
    margin-top: 1em;
    display: flex;
    justify-content: space-around;
    @media (min-width: 1025px) {
      margin-top: 0;
      width: 50%;
    }
    > button {
      min-width: 35%;
      min-height: 2.5em;
    }
  }
  @media (min-width: 1025px) {
    flex-flow: row;
  }
`
const doSearch = async ({ ref, dispatch, updateState, Query, WOEI }) => {
  const query = ref.current.value.toLowerCase().trim()
  if (!Query[query]) {
    const list = await fetchList(query)
    let whereOnEarth = {}

    const idList = list.map(place => {
      const { woeid } = place
      if (!WOEI[woeid]) {
        whereOnEarth[woeid] = place
        whereOnEarth[woeid].loaded = false
      }
      return woeid
    })
    if (Object.keys(whereOnEarth).length !== 0) {
      dispatch({
        type: UPDATE_WHERE_ON_EARTH,
        whereOnEarth,
      })
    }
    dispatch({
      type: UPDATE_QUERY,
      newQuery: {
        [query]: idList,
      },
    })
  }
  updateState(query)
}

const clearSearch = ({ ref }) => {
  ref.current.value = ""
}

const searchIfEnterIsPressed = props => ({ key }) => {
  if (key === `Enter`) {
    doSearch(props)
  }
}

function Search({ dispatch, updateState, Query, WOEI }) {
  const ref = createRef(null)
  const props = { updateState, ref, dispatch, Query, WOEI }
  return (
    <StyledSearch>
      <input ref={ref} onKeyUp={searchIfEnterIsPressed(props)} />
      <div>
        <button onClick={doSearch.bind(null, props)}>search</button>
        <button onClick={clearSearch.bind(null, props)}>clear</button>
      </div>
    </StyledSearch>
  )
}

const mapStateToProps = ({ Query, WOEI }) => ({ Query, WOEI })

export default connect(mapStateToProps)(Search)
