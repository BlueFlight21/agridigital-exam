import React from "react"
import { render, cleanup } from "react-testing-library"
import ListItem from "../ListItem"

afterEach(cleanup)

test("should render a basic list based on props", () => {
  const { getByText } = render(
    <ListItem
      title="Manila"
      locationType="City"
      lattLong={`14.609620,121.005890`}
    />,
  )
  expect(getByText("Manila")).toBeDefined()
  expect(getByText("City")).toBeDefined()
  expect(getByText("14.609620")).toBeDefined()
  expect(getByText("121.005890")).toBeDefined()
})
