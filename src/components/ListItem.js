import React from "react"
import styled from "styled-components"

const StyledListItem = styled.li`
  list-style-type: none;
  padding: 1.5em;
  color: black;
  border: thin solid #bfbfbf;
  h4 {
    margin: 0;
  }
  > div {
    display: flex;
    justify-content: space-between;
    flex-direction: row;
    > div {
      width: 50%;
      display: flex;
      flex-direction: column;
      justify-content: center;
    }
    > span {
      padding: 0.5em 0;
    }
    @media (min-width: 1025px) {
      width: 60%;
      min-height: 4em;
    }
  }
  .title {
    color: #757575;
  }
`

export default function ListItem({ title, locationType, lattLong }) {
  const [latitude, longitude] = lattLong.split(",")
  return (
    <StyledListItem>
      <div>
        <div>
          <h4>{title}</h4>
          <div>
            <span className="title">Location Type:</span> {locationType}
          </div>
        </div>
        <div>
          <div>
            <span className="title">latitude:</span>
            {latitude}
          </div>
          <div>
            <span className="title">longitude:</span>
            {longitude}
          </div>
        </div>
      </div>
    </StyledListItem>
  )
}
