const endpoints = {
  query: `api/location/search/?query=`,
  woe: `api/location/`,
}
// NOTE: Has a dev server acting as a proxy middleman to the metaWeather api
const createFetchBase = (endpoint, hasTrailing) => query =>
  fetch(`/${endpoint}${query}${hasTrailing ? `/` : ``}`).then(response =>
    response.json(),
  )

export const fetchList = createFetchBase(endpoints.query, false)
export const fetchWoe = createFetchBase(endpoints.woe, true)
