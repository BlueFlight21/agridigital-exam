import React, { Component } from "react"
import styled from "styled-components"
import { connect } from "react-redux"
import { Link } from "@reach/router"
import ListItem from "../components/ListItem"
import Search from "../components/Search"

const StyledHome = styled.div`
  ul {
    padding: 0;
    a {
      text-decoration: none;
      cursor: pointer;
    }
  }
`

class Home extends Component {
  state = { list: [] }
  updateState = term => {
    const list = this.props.Query[term].map(key => {
      const { title, location_type, woeid, latt_long } = this.props.WOEI[key]
      return {
        title,
        location_type,
        woeid,
        latt_long,
      }
    })
    this.setState({ list })
  }
  render() {
    return (
      <StyledHome>
        <Search updateState={this.updateState} />
        <ul>
          {this.state.list.map(({ title, location_type, woeid, latt_long }) => (
            <Link key={woeid} to={`${title}/${woeid}`}>
              <ListItem
                title={title}
                locationType={location_type}
                lattLong={latt_long}
              />
            </Link>
          ))}
        </ul>
      </StyledHome>
    )
  }
}

const mapStateToProps = ({ Query, WOEI }) => ({ Query, WOEI })

export default connect(mapStateToProps)(Home)
