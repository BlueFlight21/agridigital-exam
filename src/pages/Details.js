import React, { Component } from "react"
import { connect } from "react-redux"
import { fetchWoe } from "../components/Fetch"
import { Link } from "@reach/router"
import { UPDATE_WHERE_ON_EARTH_VIA_ID } from "../store/types/WOEI"
import WeatherInfo from "../components/WeatherInfo"

import styled from "styled-components"

const StyledDetails = styled.div`
  a {
    font-weight: 700;
    text-decoration: none;
    padding: 1em;
    color: #bfbfbf;
    &:hover {
      color: black;
    }
  }
  .loading {
    height: 50vh;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  .weather {
    list-style-type: none;
    display: grid;
    grid-template-columns: 1fr 1fr;
    grid-gap: 0.5em;
    padding: 0;
    @media (min-width: 1025px) {
      grid-template-columns: repeat(6, 1fr);
    }
  }
`

class Details extends Component {
  state = { loaded: false }
  async componentDidMount() {
    const { WOE, woeid, dispatch } = this.props

    if (!WOE[woeid] || !WOE[woeid].loaded) {
      const data = await fetchWoe(woeid)
      dispatch({
        type: UPDATE_WHERE_ON_EARTH_VIA_ID,
        id: woeid,
        whereOnEarth: data,
      })

      this.setState({ ...data, loaded: true })
    } else {
      this.setState({ ...WOE[woeid], loaded: true })
    }
  }
  render() {
    const { title, location_type, latt_long, consolidated_weather } = this.state
    const [latitude, longitude] = latt_long ? latt_long.split(",") : []
    return (
      <StyledDetails>
        <Link to="/"> &lt; Search</Link>
        {this.state.loaded ? (
          <div>
            <h3>{title}</h3>
            <main>
              <section>
                <div>
                  <span className="title">Type: </span> {location_type}
                </div>
              </section>
              <section>
                <div>
                  <span className="title">latitude: </span> {latitude.trim()}
                </div>
                <div>
                  <span className="title">longitude: </span> {longitude.trim()}
                </div>
              </section>
              <section>
                <h4>Weather for the next 6 days:</h4>
                <ul className="weather">
                  {consolidated_weather.map(data => (
                    <WeatherInfo key={data.id} {...data} />
                  ))}
                </ul>
              </section>
            </main>
          </div>
        ) : (
          <div className="loading">Loading</div>
        )}
      </StyledDetails>
    )
  }
}

const mapStateToProps = ({ WOEI }) => ({ WOE: WOEI })
export default connect(mapStateToProps)(Details)
