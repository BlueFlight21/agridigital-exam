import React from "react"
import { render } from "react-dom"
import Reducers from "./store/reducers"
import { createStore, compose } from "redux"
import { Provider } from "react-redux"
import App from "./App"

const store = createStore(
  Reducers,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
)

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById(`app`),
)
