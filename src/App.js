import React, { Component } from "react"
import { Router } from "@reach/router"
import styled from "styled-components"
import Home from "./pages/Home"
import Details from "./pages/Details"

const StyledApp = styled.div`
  display: flex;
  font-family: Arial, Roboto, sans-serif;
  align-items: flex-start;
  justify-content: center;
  width: 100%;
  > div {
    width: 100%;
    @media (min-width: 1025px) {
      width: 75%;
    }
  }
`

export default function App() {
  return (
    <StyledApp>
      <Router>
        <Home path="/" />
        <Details path=":title/:woeid" />
      </Router>
    </StyledApp>
  )
}
